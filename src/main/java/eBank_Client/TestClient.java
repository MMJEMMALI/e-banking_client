package eBank_Client;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.eBank_Server.persistence.Attributs;
import tn.esprit.eBank_Server.persistence.Produit_bancaire;
import tn.esprit.eBank_Server.services.IProduitBancaireService;

public class TestClient {

	public static void main(String[] args) throws NamingException, InterruptedException {

		/*String jndiName1 = "eBank-Server-ear/eBank-Server-ejb/ParticulierService!tn.esprit.eBank_Server.services.IParticulierService";

		Context ctx1 = new InitialContext();
		IParticulierService proxy1 = (IParticulierService) ctx1.lookup(jndiName1);

		String jndiName2 = "eBank-Server-ear/eBank-Server-ejb/EntrepriseService!tn.esprit.eBank_Server.services.IEntrepriseService";

		Context ctx2 = new InitialContext();
		IEntrepriseService proxy2 = (IEntrepriseService) ctx1.lookup(jndiName2);

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		Particulier po = new Particulier(789, "@esprit", 78.f, 789, "jemmali", "malek", date, "tunis", "henda", "CEO",
				99999, 8965, date, "moi meme", "canada", "celibataire", Sexe.Femme, 2, "ooo", "aaa", "vie", "adf");
		Particulier po1 = new Particulier(123, "@esprit", 100.f, 789, "aaa", "aaa", date, "aaa", "aaa", "CEO", 156, 165,
				date, "aaa", "aaaa", "celibataire", Sexe.Homme, 0, "ooo", "aaa", "aaa", "adf");
		Entreprise e = new Entreprise(826, "@live.fr", 8.f, 789, "indus", "pme", "xx", "Jemmali", 10000, 123, 15002126);

		System.out.println(
				"************************* ADD & update & delete Particulier ************************************");
		// proxy1.updateParticulier(po);
		// proxy1.updateParticulier(po1);
		// proxy1.deleteParticulier(2);

		List<Particulier> soo2 = proxy1.getAllParticulier(); // list of all

		System.out.println("The LIST of Particulier : ");
		for (Particulier s : soo2) {
			System.out.println(s.getPrenom_p());
		}

		System.out.println(
				"************************* ADD & update & delete Entreprise ************************************");
		// proxy2.updateEntreprise(e);
		// proxy2.deleteEntreprise(6);
		System.out.println(
				"************************* ADD & update & delete Credit Generique ************************************");
		String jndiName3 = "eBank-Server-ear/eBank-Server-ejb/ProduitBancaireService!tn.esprit.eBank_Server.services.IProduitBancaireService";
		Context ctx3 = new InitialContext();
		IProduitBancaireService proxy3 = (IProduitBancaireService) ctx1.lookup(jndiName3);

		Credit_generic cg = new Credit_generic(2, "credit ", "consomation", "confort plus", 60, "garantie deces", 11.f,
				4.f, 1.f, "rien", 1.f);
		// proxy3.updateProduitBancaire(cg);
		// proxy3.deleteProduit_bancaire(2);

		System.out.println(
				"************************* ADD & update & delete  compte_generique ************************************");
*/
 Produit_bancaire p= new Produit_bancaire();
 
p.setNom("compte_epargne");
p.setDescription("test");
Attributs att= new Attributs();
att.setNom("taux_intret");
att.setType("double");
att.setProduit(p);
p.getAtt().add(att);



String jndiName3 = "eBank-Server-ear/eBank-Server-ejb/ProduitBancaireService!tn.esprit.eBank_Server.services.IProduitBancaireService";
Context ctx3 = new InitialContext();
IProduitBancaireService proxy3 = (IProduitBancaireService) ctx3.lookup(jndiName3);
proxy3.updateProduitBancaire(p);
	}

}
